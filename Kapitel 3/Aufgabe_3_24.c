#include <stdio.h>

int main() {
  int auswahl, menge;
  float preis, bezahlt = 0;

  printf("Getraenke Automat v0.3\n");
  printf("Waehlen Sie ihr Getraenk aus:\n");
  printf("1) Wasser \n2) Limonade \n3) Bier\n");
  printf("Geben Sie 1, 2 oder 3 ein: ");
  scanf("%d", &auswahl);

  printf("Geben Sie die gewünschte Menge ein: ");
  scanf("%d", &menge);
  
  switch(auswahl) {
    case 1: preis = 0.5;
      break;
    case 2: preis = 1.0;
      break;
    case 3: preis = 2.0;
      break;
    default: 
      printf("Getränk nicht gefunden!");
      return 0;
  }

  printf("\n--- Bezahlvorgang ---");
  float letzteZahlung;
  while (bezahlt < preis * menge) {
    printf("\nEs fehlen noch %.2f Euro: ", preis * menge - bezahlt);
    printf("\nBitte werfen Sie ein Geldstück ein: ");
    scanf("%f", &letzteZahlung);
    bezahlt+=letzteZahlung;
  }

  printf("\n--- Getränkeausgabe ---\n");
  for(int i = 1; i<= menge; i++) 
    printf("Flasche %d von %d wurde ausgegeben.\n", i , menge);
  
  if(bezahlt == preis * menge)
    printf("\nVielen Dank, bitte entnehmen Sie %s.\n", menge > 1 ? "ihre Getränke" : "ihr Getränk");
  else
    printf("\nVielen Dank, bitte entnehmen Sie %s.\nRestgeld: %.2f Euro\n", menge > 1 ? "ihre Getränke" : "ihr Getränk", bezahlt - preis * menge);

  return 0;
}