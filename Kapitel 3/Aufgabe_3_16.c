#include <stdio.h>

int main() {
  int input, count=0;
  printf("Zahl zur Ueberpruefung eingeben: ");

  scanf("%d", &input);

  while(input%5==0) {
    count++;
    input/=5; 
  }

  printf("Die Zahl lässt sich %d mal durch 5 teilen.\n", count);
  return 0;
}