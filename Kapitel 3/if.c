#include <stdio.h>

int main() {
  int a, b;
  printf("a: ");
  scanf("%i", &a);
  printf("b: ");
  scanf("%i", &b);
  
  if(a==b) 
    printf("a ist gleich b");
  else if(a<b)
    printf("a ist kleiner b");
  else
    printf("a ist größer b");
  
  printf("\n\n%d %d\n", 2 < 3, 7!=7);
  return 0;
}