#include <stdio.h>

int main() {
  int i = 1;
  while (i <= 25) {
    printf("%d ", i);
    i++;
  }
  printf("\n");
  i = 1;
  do {
    printf("%d ", i);
    i++;
  } while (i <= 25);
  printf("\n");
  for (i=1; i<=25;i++)
    printf("%d ", i);

  return 0;
}