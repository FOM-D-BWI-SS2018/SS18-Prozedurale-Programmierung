#include <stdio.h>

int main() {
  int w;
  scanf("%i", &w);

  switch (w) {
    case 1: printf("Montag");
    break;
    case 2: printf("Dienstag");
    break;
    case 3: printf("Mittwoch");
    break;
    case 4: printf("Donnerstag");
    break;
    case 5: printf("Freitag");
    break;
    case 6: printf("Samstag");
    break;
    case 7: printf("Sonntag");
    break;
    default:
     printf("Tag nicht gefunden");
     break;
  }
  printf("\n");
  switch (w) {
    case 1:     
    case 2:     
    case 3: 
      printf("Erste Hälfte");
      break;
    case 4:     
    case 5: 
      printf("Zweite Hälfte");
      break;
    case 6:
    case 7: 
      printf("Wochenende");
      break;
    default:
     printf("Tag nicht gefunden");
     break;
  }
  printf("\n");
  return 0;
}