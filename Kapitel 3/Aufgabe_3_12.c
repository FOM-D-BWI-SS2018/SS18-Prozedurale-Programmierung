#include <stdio.h>

int main() {
  int auswahl;
  float preis, bezahlt;

  printf("Getraenke Automat v0.2\n");
  printf("Waehlen Sie ihr Getraenk aus:\n");
  printf("1) Wasser \n2) Limonade \n3) Bier\n");
  printf("Geben Sie 1, 2 oder 3 ein: ");

  scanf("%d", &auswahl);

  switch(auswahl) {
    case 1: preis = 0.5;
      break;
    case 2: preis = 1.0;
      break;
    case 3: preis = 2.0;
      break;
    default: return 0;
      break;
  }

  printf("\nBitte werfen Sie %.2f Euro ein: ", preis);
  scanf("%f", &bezahlt);

  if(bezahlt == preis)
    printf("Vielen Dank, bitte entnehmen Sie ihr Getränk.\n");
  else if (bezahlt > preis)
    printf("Vielen Dank, bitte entnehmen Sie ihr Getränk.\nRestgeld: %.2f Euro\n", bezahlt - preis);
  else
    printf("Nicht genügend eingeworfen. Entnehmen Sie ihr Geld\n");

  return 0;
}