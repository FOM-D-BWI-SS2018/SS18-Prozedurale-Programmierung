#include <stdio.h>

int main() {
  int max=0, min=0, z=1;
  int first = 1;
  while (1) {
    scanf("%d",&z);
    if(first) {
      max=min=z;
      first=0;
    }

    if(z == 0)
      break;
    if(z>max)
      max=z;
    else if (z> min)
      min = z;
  }

  printf("min: %d\nmax: %d\n", min, max);

  return 0;
}