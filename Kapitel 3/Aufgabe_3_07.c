#include <stdio.h>

int main() {
  int a, b, c, x;
  scanf("%i", &a);
  scanf("%i", &b);
  scanf("%i", &c);

  /*
  if(a < b && a < c)
    x = a;
  else if (b < a && b < c)
    x = b;
  else
    x = c;
  */

  /*
  if(b<a)
    a=b;
  if(c<a)
    a=c;
  */

  x=a<b&a<c?a:b<c?b:c;
  
  printf("Kleinste Zahl: %i", x);
  return 0;
}