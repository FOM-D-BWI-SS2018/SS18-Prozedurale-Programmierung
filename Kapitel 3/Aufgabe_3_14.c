#include <stdio.h>

int main() {
  int i = 0, j = 0;
  for(i=0; i<10; i++) {
    for(j = 0; j<10; j++)
      printf("* ");
    printf("\n");
  }
  printf("\n\n");

  i=0;
  while(i<10) {
    j=0;
    while (j<10) {
      printf("* ");
      j++;
    }
    printf("\n");
    i++;
  }
  printf("\n\n");

  i = 0;
  do {
    j=0;
    do {
      printf("* ");
      j++;
    } while(j<10);
    i++;
    printf("\n");
  } while (i<10);

  return 0;
}