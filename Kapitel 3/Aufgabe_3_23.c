#include <stdio.h>

int fibn(int n) { // exponentielle Laufzeit
  return (n == 0 || n == 1) ? n : fibn(n-2) + fibn(n-1);
}

int main() {
  int n;
  scanf("%d",&n);
  /*
  int vorvor = 0, vor = 1, fib;  
  if (n == 0 || n == 1) {
    printf("%d", n);
  }
  else {
    for(int i = 0; i < n ; i++) {     // lineare Laufzeit
      vorvor=vor;
      vor=fib;
      fib=vorvor+vor;
    }
  }
  */
  printf("%d", fibn(n));
  return 0;
}

