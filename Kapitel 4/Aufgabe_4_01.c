#include <stdio.h>

float eingabeZahl() {
  float input;
  printf("Zahl eingeben: ");
  scanf("%f", &input);
  return input;
}

float multipliziere(float a, float b) {
  return a*b;
}

float ausgabeErgebnis(float ergebnis) {
  printf("Das Ergebnis lautet %.2f.", ergebnis);
}

int main() {
  ausgabeErgebnis(multipliziere(eingabeZahl(),eingabeZahl()));
  return 0;
}