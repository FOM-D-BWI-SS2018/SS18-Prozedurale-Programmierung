#include <stdio.h>
#include "geo.h"

int main() {
  float radius;
  scanf("%f", &radius);

  printf("Kreisumfang: %.2f", kreisumfang(radius));
  printf("\nFlächeninhalt: %.2f", flaecheninhalt(radius));
  return 0;
}