#include <math.h>

float kreisumfang(float radius) {
  return radius * 2 * M_PI;
}

float flaecheninhalt(float radius) {
  return radius * radius * M_PI;
}