#include <stdio.h>
#include <string.h>

typedef struct {
  char sorte[20];
  float preis;
  int kilo;
} APFEL;

int main() {
  APFEL daten[3] = {
    {"Jonagold", 1.49, 29},
    {"Delicious", 1.59, 15},
    {"Elstar", 1.99, 35},
  };

  float gesamt = 0;  
  printf("%-10s\t%s\t%s\n", "Sorte", "Preis", "Absatz");

  for(int i = 0; i < 3; i++) {
    gesamt += daten[i].preis * ((float) daten[i].kilo) ;
    printf("%-10s\t%-5.2f\t%-5i\n", daten[i].sorte, daten[i].preis, daten[i].kilo);
  }
  
  printf("\nGesamtumsatz:\t%.2f\n", gesamt);

}