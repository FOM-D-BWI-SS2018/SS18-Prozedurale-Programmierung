#include <stdio.h>
#include <string.h>

int main() {
  char path[] = "/Users/mwa/Documents/Repositories/bitbucket/fom-prozedurale-programmierung/Kapitel 8/Aufgabe_8_04.c";

  char *extension = strrchr(path,'.') + 1;  
  char *filename = strrchr(path, '/') + 1;
  *--filename = '\0';

  printf("Extension: %s\n", extension);
  printf("Filename: %s\n", filename);
  printf("Direcotry: %s\n", path);

  return 0;
}