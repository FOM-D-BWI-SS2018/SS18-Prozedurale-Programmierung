#include <stdio.h>

int main(int argc, char const *argv[])
{
  int a = 647, b = 223;
  int ggt = 0;
  int maxGgt = a < b ? a : b;

  for(int i = 1; i < maxGgt; i++) {
    if(a % i == 0 && b % i == 0)
      ggt = i;
  }

  printf("%d\n", ggt);
  return 0;
}
