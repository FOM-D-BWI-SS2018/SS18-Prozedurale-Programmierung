#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
  char string[] = "Paris 111,Berlin 6,London 16,Rom 28";
  char delimiter[] = ",";
  char *ptr = strtok(string, delimiter);

  int sum = 0;
  while(ptr != NULL) {
    printf("%s\n", ptr);
    sum += atoi(strrchr(ptr, ' '));
    ptr = strtok(NULL, delimiter);
  }
  
  printf("Summe: %d\n", sum);

  return 0;
}