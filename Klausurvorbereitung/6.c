#include <stdio.h>


int main(int argc, char const *argv[])
{
  int a = 5;

  int *ptr = &a;

  printf("adress of a: %i\n", ptr);
  printf("content of a: %d\n", *ptr);
  printf("adress of ptr: %i\n", &ptr);
  return 0;
}
