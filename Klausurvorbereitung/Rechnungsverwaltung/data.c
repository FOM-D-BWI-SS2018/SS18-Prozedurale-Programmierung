#include <stdio.h>

typedef struct {
  int id;
  char *name;
} customer;

typedef struct {
  char *name;
  float price;
  int amount;
} position;

typedef struct {
  char *date;
  customer customer;
  position positions[];
} bill;