#include <stdio.h>


int main(int argc, char const *argv[])
{
  int hour;
  scanf("%d", &hour);
  if(hour < 1 || hour >= 24)
    printf("Keine valide Uhrzeit");
  else if(hour < 6 || hour > 22)
    printf("%d Uhr -> Nacht", hour);
  else if(hour == 12)
    printf("%d Uhr -> Mahlzeit", hour);
  else 
    printf("%d Uhr -> Tag", hour);
    
  printf("\n");
  return 0;
}
