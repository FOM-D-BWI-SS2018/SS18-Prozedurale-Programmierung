#include <stdio.h>

int main()
{
  int base;
  scanf("%d", &base);

  for(int i = base; i > 0 ; i--) {
    int fakultaet = i;
    printf("%d! = %d", i, i);
    for(int j = i - 1; j > 0 ; j--) {
      printf(" * %d", j);
      fakultaet *= j;
    }
    printf(" = %d\n", fakultaet);
  }
  return 0;
}