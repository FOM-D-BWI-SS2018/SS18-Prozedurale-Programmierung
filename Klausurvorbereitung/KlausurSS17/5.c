#include <stdio.h>


int main(int argc, char const *argv[])
{
  for(int i = 11; i <= 20; i++) {
    printf("%d-er Reihe:", i);
    
    for(int j = 1; j <= 10; j++)
    {
      printf(" %d", i*j);
    }
    printf("\n");
  }
}