#include <stdio.h>

int main() {
  int array[8][8] = {};
  
  for (int i = 0; i < 64; i++){
    *((int *) array + i) = i + 1;
  }
  
  int *ptr = &(array[0][2]);
  for(int i = 2; i < 64; i=i+3) {
    *ptr = 0;
    ptr += 3;
  }

  for (int i = 0; i < 8; i++) {
    for(int j = 0 ; j < 8; j++)
      printf("%2d ", array[i][j]);
    printf("\n");
  }
  return 0;
}