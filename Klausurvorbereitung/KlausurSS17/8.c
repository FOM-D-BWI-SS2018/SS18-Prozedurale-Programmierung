#include <stdio.h>
void addiere(int *a, int *b, int *sum ){
  *sum = *a+*b;
}
int main(){
 int zahl1 = 10;
 int zahl2 = 8;
 int summe;

 // Funktionsaufruf implementieren
  addiere(&zahl1, &zahl2, &summe);
 printf("Summe von zahl1 und zahl2: %i\n", summe);
 return 0;
}