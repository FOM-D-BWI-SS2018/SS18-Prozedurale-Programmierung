#include <stdio.h>


int main(int argc, char const *argv[])
{
  int x; float y; char s[20];

  scanf("%d", &x);
  scanf("%f", &y);
  scanf("%s", s);

  printf("%05d\n", x);
  printf("%7.2f\n", y);
  printf("%-30s\n", s);

  return 0;
}
