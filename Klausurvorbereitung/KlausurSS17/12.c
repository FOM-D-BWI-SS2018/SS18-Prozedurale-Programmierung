#include <stdio.h>

struct kunde {
  int id;
  char *name;
};

struct rechnung
{
  struct kunde k;
  int id;
};

int main() {
  struct rechnung r;
  r.k.id = 2;
  r.k.name = "Peter";
  r.id = 233;
  
  printf("%d %s\n%d", r.k.id, r.k.name, r.id);

  return 0;
}