#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
  FILE *fp;
  float bestShare = 0;
  char best[30];
  char puffer[30];

  if((fp = fopen("browser.txt", "r")) != NULL) {
    while(fgets(puffer, 30, fp)) {
      char *browser = strtok(puffer, ";");
      float share = atof(strtok(NULL, ";"));
      if(bestShare < share) {
        bestShare = share;
        strcpy(best, browser);
      }
    }
    printf("Bester Browser ist %s mit %.2f Marktanteil\n", best, bestShare);
    fclose(fp);
  } else {
    printf("Fehler beim öffnen\n");
  }

  return 0;
}
