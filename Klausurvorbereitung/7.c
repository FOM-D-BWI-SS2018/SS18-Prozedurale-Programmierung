#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct kunde {
  int id;
  char vorname[35];
  char *nachname;
};

struct rechnung {
  int id;
  struct kunde kunde;
  float preis;
};

void ausgeben(struct rechnung *r) {
  printf("Id: \t\t%d\n", r->id);
  printf("Preis: \t\t%.2f\n", r->preis);
  printf("K Id: \t\t%d\n", r->kunde.id);
  printf("K Vorname: \t%s\n", r->kunde.vorname);
  printf("K Nachname \t%s\n", r->kunde.nachname);
}

void einlesen(struct rechnung *r) {
  printf("Id\n");
  scanf("%d", &r->id);
  printf("K Id\n");
  scanf("%d", &r->kunde.id);
  printf("Preis\n");
  scanf("%f", &r->preis);
  printf("K Vorname\n");
  scanf("%s", &(r->kunde.vorname[0]));
  printf("K Nachname\n");
  r->kunde.nachname = malloc(sizeof(char) * 25);
  scanf("%24s", r->kunde.nachname);
}

int main(int argc, char const *argv[])
{
  struct rechnung r, r2;
  r.id = 32;
  r.preis = 20.0;
  r.kunde.id = 232;  
  strcpy(r.kunde.vorname, "Hans");

  r.kunde.nachname = "Peters";

  ausgeben(&r);
  printf("\n\nEnter: \n");
  einlesen(&r2);
  printf("\n\nEntered following\n");
  ausgeben(&r2);

  return 0;
}


