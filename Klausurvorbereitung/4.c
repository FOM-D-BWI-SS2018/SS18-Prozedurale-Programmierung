#include <stdio.h>


int main(int argc, char const *argv[])
{
  
  for(int i = 0; i < 10; i++)
  {    
    for(int j = 0; j < 10; j++)
    {
      printf(" %c ", i == j ? '*' : ' ');
    }
    printf("\n");
  }
  
  for(int i = 1; i <= 10; i++)
  {    
    for(int j = 1; j <= 10; j++)
    {
      printf("%i*%i=%i\n", i,j, i*j);
    }
    printf("\n\n");
  }

  int base = 5;
  int fak = 0;

  for(fak = base--; base > 1; base--) {
    fak += fak * (base - 1);
  }
  printf("fakultät von base: %d\n", fak);
  return 0;
}
