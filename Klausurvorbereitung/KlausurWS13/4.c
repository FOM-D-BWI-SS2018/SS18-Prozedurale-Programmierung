#include <stdio.h>


int main(int argc, char const *argv[])
{
  int note;
  scanf("%d", &note);
  printf("if:\n");
  if(!(note > 0 && note <= 6))
    printf("note nicht zwischen 1 und 6");
  else if(note <= 2)
    printf("top");
  else if(note <= 4)
    printf("bestanden");
  else
    printf("nicht bestanden");

  printf("\nswitch:\n");

  switch(note) {
    case 1:
    case 2: 
      printf("top");
      break;
    case 3:
    case 4:
      printf("bestanden");
      break;
    case 5:
    case 6:
      printf("nicht bestanden");
      break;
    default:
      printf("note nicht zwischen 1 und 6");
  }

  printf("\n");
  return 0;
}
