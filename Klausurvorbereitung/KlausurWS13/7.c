#include <stdio.h>

float getMinV(float a, float b, float c) {
  return a < b && a < c ? a : b < a && b < c ? b : c; 
}


void getMinR(float *a, float *b, float *c, float *min) {
  *min = *a < *b && *a < *c ? *a : *b < *a && *b < *c ? *b : *c;
}


int main(int argc, char const *argv[])
{
  float a = 3, b = 2, c = 1, min=-1;

  printf("%.2f\n", getMinV(a,b,c));

  getMinR(&a, &b, &c, &min);

  printf("%.2f\n",min);

  return 0;
}
