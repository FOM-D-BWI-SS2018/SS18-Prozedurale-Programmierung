#include <stdio.h>


int main(int argc, char const *argv[])
{
  printf("Ausgabe:\n");
  int line = 1;

  while(line <= 10) {
    printf("Zeile %2i: ", line);
    for(int i = 0; i < line*2; i++)
      printf("*");
    printf("\n");
    line++;
  }

  return 0;
}
