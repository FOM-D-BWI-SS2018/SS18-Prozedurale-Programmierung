#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char const *argv[])
{
  FILE *fp = fopen("./data/ergebnis.txt", "r");
  int sum = 0, count = 0;

  char line[80];
  if(fp != NULL) {
    while(fgets(line, 80, fp) != NULL) {
      sum+=atoi(strtok(line, ":"));
      count++;
    }
    printf("Summe:\t%d\n", sum);
    printf("Anzahl:\t%d\n", count);
    printf("Durchschnitt:\t%.2f\n", ((float) sum) / ((float) count));
  } else {
    printf("Fehler beim öffnen!\n");
  }

  return 0;
}
