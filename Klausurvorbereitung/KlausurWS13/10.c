#include <stdio.h>

struct app {
  char bezeichnung[25];
  char hersteller[25];
  float preis;
};

void einlesen(struct app*);
void ausgeben(struct app);

int main(int argc, char const *argv[])
{
  struct app app;

  einlesen(&app);
  ausgeben(app);
  printf("\n");
  return 0;
}

void einlesen(struct app* app) {
  scanf("%s", app->bezeichnung);
  scanf("%s", app->hersteller);
  scanf("%f", &(app->preis));
}

void ausgeben(struct app app) {
  printf("%s\n", app.bezeichnung);
  printf("%s\n", app.hersteller);
  printf("%.2f\n", app.preis);
}