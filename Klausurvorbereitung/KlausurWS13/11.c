#include <stdio.h>


int main(int argc, char const *argv[])
{
  int array[8][8] = {};

  int *ptr = array;
  for(int i = 0; i < 8; i++) {
    *ptr = 1;
    ptr += 9;
  }
  
  // Ausgabe nicht relevant für Aufgabenstellung
  for(int i = 0; i < 8; i++) {
    for(int j = 0 ; j < 8 ; j++)
      printf("%d", array[i][j]);
    printf("\n");
  }
  return 0;
}
