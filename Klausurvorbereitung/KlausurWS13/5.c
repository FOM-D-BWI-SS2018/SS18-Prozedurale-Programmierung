#include<stdio.h>
#include<string.h>

int main() {
  char string[] = "15.06.2012#08:15:00#Zahnarzttermin#privat"; 
 
  char *date = strtok(string, "#");
  char *time = strtok(NULL, "#");
  char *minute, *hour;
  char *termin = strtok(NULL, "#");
  char *type = strtok(NULL, "#");

  hour = strtok(time, ":");
  minute = strtok(NULL, ":");
  
  printf("Ausgabe:\n");
  printf("Datum: %s\n", date);
  printf("Uhrzeit: %s:%s\n", hour, minute);
  printf("Termin: %s\n", termin);
  printf("Typ: %s\n", type);

  return 0;
}
