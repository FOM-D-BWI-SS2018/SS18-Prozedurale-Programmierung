#include <stdio.h>

#define MULTI(a,b) (a*b)
#define ADD(a,b) (a+b)

int main(int argc, char const *argv[])
{
  int a = ADD(1,3);

  printf("a*a=%d*%d=%d\n",a,a, MULTI(a,a));
  return 0;
}
