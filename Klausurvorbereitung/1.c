#include <stdio.h>

int main(int argc, char const *argv[])
{
  char text[] = "Hallo Welt!";
  float num = 2.34235;

  printf("%-20s <- text\n", text);
  printf("%20s <- text\n", text);

  printf("%-7.2f <- num\n", num);
  return 0;
}
