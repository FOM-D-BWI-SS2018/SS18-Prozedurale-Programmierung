#include <stdio.h>
#include <string.h>

int main(int argc, char const *argv[])
{
  char data[] = "18:00;root;java -jar x.jar -Dchangelog.date=4443";
  printf("%s\n\n", data);

  printf("time: %s\n", strtok(data, ";"));
  printf("user: %s\n", strtok(NULL, ";"));
  printf("cmd: %s\n", strtok(NULL, ";"));
  return 0;
}