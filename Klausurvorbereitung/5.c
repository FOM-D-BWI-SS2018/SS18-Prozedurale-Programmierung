#include <stdio.h>

void increment(int *n) {
  *n = *n + 1;
}

int decrement(int n) {
  return --n;
}

int main(int argc, char const *argv[])
{
  int a = 5;

  // call by reference:
  increment(&a);
  printf("%d",a);

  // call by value:
  a = decrement(a);
  printf("%d",a);
  
  printf("\n");
  return 0;
}
