#include <stdio.h>

int main() {
  int days;
  printf("Geben Sie die Anzahl der Tage ein:");
  scanf("%i",&days);

  printf("Stunden: %i\nMinuten: %i\nSekunden: %i\n",days*24, days*24*60, days*24*60*60);

  return 0;
}