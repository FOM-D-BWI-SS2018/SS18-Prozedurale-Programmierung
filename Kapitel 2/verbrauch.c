#include <stdio.h>

int main() {
  float verbrauch, strecke;
  printf("Taschenrechner \n");

  printf("Geben Sie den Verbrauch ein (Lieter): ");
  scanf("%f", &verbrauch);

  printf("Geben Sie die gefahrene Strecke ein (KM): ");
  scanf("%f", &strecke);
  
  printf("Die Summe ergibt: %.2f\n", verbrauch*100/strecke);
  return 0;
}