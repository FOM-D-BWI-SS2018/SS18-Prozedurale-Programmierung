#include <stdio.h>

int main() {
  int sekunden, minuten, stunden, tage;

  printf("Geben Sie die Anzahl der Sekunden ein: ");
  scanf("%i",&sekunden);

  tage = sekunden / (24*60*60);
  sekunden %= (24*60*60);

  stunden = sekunden / (60*60);
  sekunden %= (60*60);

  minuten = sekunden / 60;
  sekunden %= 60;
  
  printf("Tage: %i\nStunden: %i\nMinuten: %i\nSekunden: %i\n",tage,stunden,minuten,sekunden);
  return 0;
}