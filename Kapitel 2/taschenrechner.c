#include <stdio.h>

int main() {
  float a, b;
  printf("Taschenrechner \n");

  printf("Geben Sie die 1. Zahl ein: ");
  scanf("%f", &a);

  printf("Geben Sie die 2. Zahl ein: ");
  scanf("%f", &b);
  
  printf("Die Summe ergibt: %.2f\n", a + b);
  return 0;
}