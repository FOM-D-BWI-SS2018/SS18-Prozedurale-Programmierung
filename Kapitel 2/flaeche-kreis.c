#include <stdio.h>
#include <math.h>

int main() {
  float radius;
  printf("Geben Sie den Radius ein: ");
  scanf("%f", &radius);

  printf("Der Flächeninhalt beträgt %.2f\n", radius * radius * M_PI);
  return 0;
}