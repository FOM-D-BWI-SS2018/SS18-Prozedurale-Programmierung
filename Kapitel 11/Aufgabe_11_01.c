#include <stdio.h>

#define ADD(a,b) (a + b)
#define MULT(a,b) ((a) * (b))

int main() {
  int add = ADD(1,3) * ADD(2,4);
  printf("ADD = %d\n", add);

  int mult = MULT(1+2,5+1);
  printf("MULT = %d\n", mult);
  return 0;
}