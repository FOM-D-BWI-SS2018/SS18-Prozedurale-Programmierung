#include <stdio.h>

#define MAX(a,b) (a > b ? a : b)

int main() {
  int ia = 4, ib = 7;
  printf("%d\n", MAX(ia,ib));
  float fa = 12.6, fb = 0.8 ;
  printf("%f\n", MAX(fa,fb));
  return 0;
}