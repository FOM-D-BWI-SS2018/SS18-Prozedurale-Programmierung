#include <stdio.h>
#include <stdlib.h>

float getMin(int* array, int* size) {
  float min = *array;
  for(int i = 0; i < *size; i++) {
    if(array[i] < min)
      min = array[i];
  }
  return min;
}

int main() {
  int *array, size;
  printf("Array size: ");
  scanf("%d", &size);

  array = (int*) calloc(size, sizeof(int));

  for(int i = 0; i < size; i++) {
    scanf("%d", &array[i]);
  }

  printf("Minimum: %.2f\n", getMin(array, &size));
  return 0;
}