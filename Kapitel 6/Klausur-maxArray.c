#include <stdio.h>

float getMax(float array[], int anzahl) {
  float max = array[0];
  for(int i = 0; i < anzahl; i++) {
    max = array[i] > max ? array[i] : max; 
  }
  return max;
}

int main() {
  float array[9] = { 1, 2, 3, 4, 5, 3, 6, 2, 1};
  printf("%.2f\n", getMax(array, sizeof(array)/sizeof(array[0])));
  return 0;
}

