#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
  FILE *f;

  char zeile[21], hersteller[21];
  int kmh;
  int gesamt = 0, anzahl = 0;

  if((f=fopen("auto.txt", "r")) != NULL) {
    while(fscanf(f, "%s", zeile) != EOF) {
      gesamt += atoi(strrchr(zeile, ':') + 1);;
      anzahl++;
    }

    printf("Gesamt:\t\t%d\n", gesamt);
    printf("Anzahl:\t\t%d\n", anzahl);
    printf("Schnitt:\t%.2f\n", (float) gesamt / anzahl);
    fclose(f);
  } else {
    printf("Fehler beim Oeffnen\n");
  }
  
  printf("\n");
}