#include <stdio.h>
#include <string.h>

int main() {
  FILE *f;
  char date[11], time[6], name[20];
  if((f=fopen("log.csv", "r")) != NULL) {
    while(fscanf(f, "%10s,%5s,%s", &date, &time, &name) != EOF) {
      printf("Datum:\t\t%s\n", date);
      printf("Uhrzeit:\t%s\n", time);
      printf("User:\t\t%s\n", name);
      printf("-----------------------------\n");
    }
    fclose(f);
  } else {
    printf("Fehler beim Oeffnen\n");
  }

  printf("\n");
}