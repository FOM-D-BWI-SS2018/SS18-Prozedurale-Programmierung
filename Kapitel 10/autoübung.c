#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char const *argv[])
{
  FILE *fp;
  int fastest = 0;
  char fastestName[30];
  char puffer[30];
  if((fp = fopen("auto.txt", "r")) != NULL) {
    while(fgets(puffer, 30, fp)) {
      char *name = strtok(puffer, ":");
      int speed = atoi(strtok(NULL, ":"));

      if(fastest < speed) {
        fastest = speed;
        strcpy(fastestName, name);
      }
    }
    printf("%s %d", fastestName, fastest);
  } else printf("Fehler beim öffnen!");

  return 0;
}
