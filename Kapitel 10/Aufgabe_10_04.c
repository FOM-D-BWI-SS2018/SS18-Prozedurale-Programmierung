#include <stdio.h>

int main() {
  FILE *csv, *copy;
  char buffer[80];

  csv=fopen("log.csv", "r");
  copy=fopen("copylog.csv", "w");

  if(csv != NULL && copy != NULL) {
    while(fgets(buffer, 80, csv)) {
      fputs(buffer, copy);
    }
    fclose(csv);
    fclose(copy);    
  } else {
    printf("Fehler beim Oeffnen\n");
  }
  printf("\n");
}