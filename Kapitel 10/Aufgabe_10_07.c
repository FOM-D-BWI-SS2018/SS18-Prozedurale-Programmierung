#include <stdio.h>
#include <string.h>
#include <stdlib.h>

const int name = 15, firstname = 15, mail = 30, code = 2;
const int line = name + firstname + mail + code + 1; // 1 for LF lineending

int main() {
  FILE *flat;
  if((flat=fopen("flatFile.txt", "r+")) != NULL) {

    fseek(flat,2*line+name,SEEK_SET);
    fprintf(flat, "%-15s", "Martina");

    fseek(flat,2*line+name+firstname,SEEK_SET);
    fprintf(flat, "%-30s", "m.hill@gmx.de");

    fclose(flat);
  } else {
    printf("Fehler beim Oeffnen\n");
  }
}