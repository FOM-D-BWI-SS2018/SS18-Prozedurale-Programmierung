#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main() {
  FILE *csv;
  char buffer[80], *temp;
  float sum = 0, count = 0;
  if((csv=fopen("wetter.txt", "r")) != NULL) {
    while(fgets(buffer, 80, csv)) {
      temp = strchr(buffer, ':') + 1;
      sum += atof(temp);
      count++;
    }
    printf("Sum: %.2f\n", sum);
    printf("Count: %.0f\n", count);
    printf("Avg: %.2f\n", sum / count);

    fclose(csv);
  } else {
    printf("Fehler beim Oeffnen\n");
  }
}