#include <stdio.h>
#include <string.h>

int main() {
  FILE *csv;
  char buffer[80], *date, *time, *user;
  if((csv=fopen("log.csv", "r")) != NULL) {
    while(fgets(buffer, 80, csv)) {
      date = strtok(buffer, ",");
      time = strtok(NULL, ",");
      user = strtok(NULL, ",");

      printf("Datum:\t\t%s\n", date);
      printf("Uhrzeit:\t%s\n", time);
      printf("User:\t\t%s", user);
      printf("\n----------------------------------\n");
    }
    fclose(csv);
  } else {
    printf("Fehler beim Oeffnen\n");
  }
}