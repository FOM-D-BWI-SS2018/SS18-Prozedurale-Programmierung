#include <stdio.h>
#include <string.h>

int main() {
  FILE *f;
  int numbers[5] = {1,2,3,4,5};
  
  if((f=fopen("data.txt", "w")) != NULL) {    
    for(int i = 0; i < 5; i++)
    {
      fprintf(f, "%d ", numbers[i]);
    }    
    fclose(f);
  } else {
    printf("Fehler beim öffnen\n");
  }

  int temp = 0;
  if((f=fopen("data.txt", "r")) != NULL) {
    while(fscanf(f, "%d ", &temp) != EOF) {
      printf("%d\n", temp);
    }
    fclose(f);
  } else {
    printf("Fehler beim öffnen\n");
  }

  printf("\n");
}